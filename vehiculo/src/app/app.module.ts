import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule  } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { OrderModule } from 'ngx-order-pipe';
import  { NgxPaginationModule }  from  'ngx-pagination' ;
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VehiculoListComponent } from './components/vehiculo-list/vehiculo-list.component';
import { VehiculoFormComponent } from './components/vehiculo-form/vehiculo-form.component';

import { VehiculosService}from './services/vehiculos.service';
import { FilterPipe } from './pipe/filter.pipe' 
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    VehiculoListComponent,
    VehiculoFormComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    AppRoutingModule,
    HttpClientModule,
    OrderModule,
    CommonModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule


  ],
  providers: [VehiculosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
