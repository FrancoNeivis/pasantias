import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Vehiculo} from '../models/vehiculo';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class VehiculosService {

  
    
  URL='http://192.168.100.109:5555/vehiculos'

  constructor(private http:HttpClient) {}

  //metodos para los vehiculos

  getVehiculos() {
    return this.http.get(`${this.URL}/consultarVehiculos`);
  }

  getVehiculo(id: string ) {
    return this.http.get(`${this.URL}/consultarVehiculoId/${id}`);
  }

  putVehiculo(id: string, putVehiculo:Vehiculo){
    return this.http.put(`${this.URL}/eliminarVehiculos/${id}`, putVehiculo);
  }

  putVehiculoEstado(id: string, putVehiculoEstado:Vehiculo){
    return this.http.put(`${this.URL}/actualizarEstadoVehiculo/${id}`, putVehiculoEstado);
  }


  postVehiculo(vehiculo: Vehiculo){
    return this.http.post(`${this.URL}/ingresarVehiculos`, vehiculo);

  }

  updateVehiculo( id: string|number, updatedVehiculo: Vehiculo )
  {
    return this.http.put(`${this.URL}/actualizarVehiculos/${id}`, updatedVehiculo);
  }
  

}
