import { HttpClient } from '@angular/common/http';
import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';
import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router'
import { NgSelectConfig } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';

import { Vehiculo } from 'src/app/models/vehiculo';
import { WebService } from 'src/app/models/webservice';
import Swal from 'sweetalert2';
import {VehiculosService} from '../../services/vehiculos.service'
import {FormGroup, Validators, FormControl, FormBuilder, AbstractControl} from '@angular/forms';


@Component({
  selector: 'app-vehiculo-form',
  templateUrl: './vehiculo-form.component.html',
  styleUrls: ['./vehiculo-form.component.css']
})
export class VehiculoFormComponent implements OnInit {

  conversion:any;
  rta:any = [];
  form!: FormGroup;


  @HostBinding('class') classes = 'row' 
  
  webservice:WebService = {
    id: '',
    nombre: '',
    ci_ruc: ''
  }

  vehiculo:Vehiculo = {

    id: 0,
    chofer: ' ',
    oficial: ' ',
    bultos: 0,
    placa: ' ',
    idproveedor: 0,
    nota: 'N/A',
    //fecha: new Date (), 
    //horaEntrada:  ' ',
    //horaIngreso: Time,
    //horaSalida:Time
  };
   edit: boolean = false;


  constructor( private vehiculosService: VehiculosService, private router:Router, private activatedRoute: ActivatedRoute, private http:HttpClient, private config: NgSelectConfig, private formBuilder: FormBuilder) {
    this.config.notFoundText = 'Custom not found';
    this.config.appendTo = 'body';
    this.config.bindValue = 'value';
    this.buildForm();
  
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      chofer: ['', [Validators.required, Validators.minLength(10)]],
      oficial: ['', [Validators.required, Validators.minLength(10)]],
      placa: ['', [Validators.required, Validators.minLength(8)]],
      bultos: ['', [Validators.required,Validators.pattern("^[0-9]*$"),Validators.minLength(100)]]
    });
  }


  
  save(event: Event){
    event.preventDefault();
    if (this.form.valid) {
      const value=this.form.value;
      console.log(value)
    }
    
  }
  
  ngOnInit(): void { 
    this.http.get('https://wserver.grupozurita.com.ec/bodega/obtenerProveedores/1/false')
    .subscribe((data:any) =>{
      this.conversion = data.rows;
      this.rta = this.conversion;
      console.log('prueba',this.rta);
      
    })




    const params = this.activatedRoute.snapshot.params;
    if (params.id){
      this.vehiculosService.getVehiculo(params.id)
      .subscribe(
        res =>{
          console.log(res)
         // this.updateVehiculo()
          //this.edit=true;
       
          
        },
        err => console.error(err)
      )
    }
    }


    
  
  saveNewVehiculo () {
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: '¡Registro Guardado!',
      showConfirmButton: false,
      timer: 1500
    })
    
    this.vehiculosService.postVehiculo(this.vehiculo)
    .subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/vehiculos']);
      },
      err => console.error(err)
    )
    
  }


  updateVehiculo(){
  

    this.vehiculosService.updateVehiculo(this.vehiculo.id, this.vehiculo)
    .subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/vehiculos']);
      },
      err => console.log(err)
    )
  }

  }
