import { Component, OnInit } from '@angular/core';
import {VehiculosService} from '../../services/vehiculos.service'
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { OrderPipe } from 'ngx-order-pipe'      ;

@Component({
  selector: 'app-vehiculo-list',
  templateUrl: './vehiculo-list.component.html',
  styleUrls: ['./vehiculo-list.component.css'],

})

export class VehiculoListComponent implements OnInit {

  conversion:any;
  rta:any = [];

  pageActual: number = 1;
  condicion = 2;
  condicion1=0;
  condicion2=0;
  clave: string = 'chofer';
  reversa: boolean = false;

  filterVehiculo = '';
  filterFecha = '';
  vehiculos: any = [];

  constructor(private vehiculosServices:VehiculosService, orderPipe :  OrderPipe, private http:HttpClient) {
  
  }

  ngOnInit(): void { 
    this.http.get('https://wserver.grupozurita.com.ec/bodega/obtenerProveedores/1/false')
    .subscribe(data=>{
      this.conversion = data;
      this.rta = this.conversion;
      console.log('prueba',this.rta);
      
    })
    this.getVehiculos();
   };

   ordenar(clave: string){
     if (this.clave === clave) {
       this.reversa = ! this.reversa;
      
     }
     this.clave = clave;
   }

  getVehiculos(){ 
    this.vehiculosServices. getVehiculos() .subscribe(
      (res:any) => {
         this.vehiculos = res.vehiculo;
         console.log(res.vehiculo)
       },
      err => console.error(err)
    )
  }

  eliminarVehiculo(id: string){
    Swal.fire({
      title: '¿Deseas eliminar el registro?',
      text: "Al eliminar el registro no podrás visualizarlo",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.vehiculosServices.putVehiculo(id, this.vehiculos).subscribe(
          res => {
            console.log(res)
            this.getVehiculos();
          },
          err => console.log(err)
        )
      }
    })
  }
  
  actualizarEstado(id: string){
    
    this.vehiculosServices.putVehiculoEstado(id, this.vehiculos).subscribe(
      res => {
        console.log(res)
        this.getVehiculos();
        
      },
      err => console.log(err)
    )  
  }  
}
