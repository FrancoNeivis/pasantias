import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiculoListComponent } from './components/vehiculo-list/vehiculo-list.component';
import { VehiculoFormComponent } from './components/vehiculo-form/vehiculo-form.component'


const routes: Routes = [
  {
    path:' ',
    redirectTo: '/',
    pathMatch:'full'
  },
  {
    path:'',
    component:VehiculoListComponent
    
    
  },
  {
    path: 'vehiculos/add',
    component: VehiculoFormComponent
  },
  {
    path:'vehiculos/edit/:id',
    component: VehiculoFormComponent

  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
