import { Time } from "@angular/common";


export interface Vehiculo{

    id: number;
    chofer: string;
    oficial: string;
    bultos: number;
    placa: string;
    idproveedor: number;
    nota:string;
    //fecha:Date;
    //horaEntrada: Time;
    //horaIngreso: Time;
    //horaSalida: Time
}