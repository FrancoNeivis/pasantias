'use strict'

const Helpers = use('Helpers')
const Env = use('Env')

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Default disk
  |--------------------------------------------------------------------------
  |
  | The default disk is used when you interact with the file system without
  | defining a disk name
  |
  */
  default: 'local',

  disks: {
    /*
    |--------------------------------------------------------------------------
    | Local
    |--------------------------------------------------------------------------
    |
    | Local disk interacts with the a local folder inside your application
    |
    */
    local: {
      root: '//192.168.100.2/imagenes',
      driver: 'local'
    },

    uploads: {
      root: './tmp/uploads',
      driver: 'local'
    },

    despachos_suc: {
      root: './tmp/uploads/despachos_suc',
      driver: 'local'
    },

    vehiculos: {
      root: './tmp/uploads/vehiculos',
      driver: 'local'
    },

    proformas: {
      root: '/home/webadmin/uploads/proformas',
      driver: 'local'
    },

    despachos: {
      root: '/home/webadmin/uploads/despachos',
      driver: 'local'
    },

    visitas_ext: {
      root: '/home/webadmin/uploads/visitas_ext',
      driver: 'local'
    },

    solicitudes: {
      root: '/home/webadmin/uploads/solicitudes',
      driver: 'local'
    },

    /*
    |--------------------------------------------------------------------------
    | S3
    |--------------------------------------------------------------------------
    |
    | S3 disk interacts with a bucket on aws s3
    |
    */
    s3: {
      driver: 's3',
      key: Env.get('S3_KEY'),
      secret: Env.get('S3_SECRET'),
      bucket: Env.get('S3_BUCKET'),
      region: Env.get('S3_REGION')
    }
  }
}
