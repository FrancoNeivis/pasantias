'use strict'

const Model = use('Model')

class RolesUser extends Model {

    static get table () {
        return 'sysweb.roles_users'
      }

    // users () {
    //     return this.hasMany('App/Models/User')
    //   }

    // roles () {
    //     return this.hasMany('App/Models/Role')
    //   }

}

module.exports = RolesUser
