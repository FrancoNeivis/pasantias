'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Calificacion extends Model {

    static get table () {
        return 'calificacion.calificaciones'
      }

      static get primaryKey()
      {
          return 'id_calificaciones'
      }
}

module.exports = Calificacion
