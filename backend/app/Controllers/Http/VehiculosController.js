'use strict'

const Database = use('Database')
const moment = require('moment')
const Env = use('Env')
const Drive = use('Drive')

class VehiculosController{

    //Ingresar
    async ingresarVehiculos({request, params, response}){

        try {
            
            let chofer = request.input('chofer').toUpperCase();
            let oficial = request.input('oficial').toUpperCase();
            let bultos = request.input('bultos');
            let placa = request.input('placa').toUpperCase();
            let idproveedor = request.input('idproveedor');
            let nota = request.input('nota').toUpperCase();
            let fecha = moment().format('YYYY-MM-DD');
            let horaentrada = moment().format('HH:mm:ss');
            let horaingreso = moment().format('HH:mm:ss');
            let horasalida = moment().format('HH:mm:ss');
            let estado = 1;
            let estadoVehiculo = 0;
            
            const existe = await Database.raw("select id, chofer, oficial, bultos, placa, idproveedor, nota, fecha, horaentrada, horaingreso, horasalida, estado, estadoVehiculo from registrovehiculos where estado='"+estado+"' and placa='"+placa+"' and chofer='"+chofer+"' and horaentrada='"+horaentrada+"'")

            if(existe[0].length >= 1){
                return response.status(200).send({message: 'El vehiculo que intenta ingresar ya ha sido registrado'})

            }
            else { 

                const vehiculo = await Database.raw("insert into registrovehiculos (chofer, oficial, bultos, placa, idproveedor, nota, fecha, horaentrada, horaingreso, horasalida, estado, estadoVehiculo) values ('"+chofer+"', '"+oficial+"', '"+bultos+"', '"+placa+"' , '"+idproveedor+"', '"+nota+"' , '"+fecha+"', '"+horaentrada+"', '"+horaingreso+"', '"+horasalida+"', '"+estado+"', '"+estadoVehiculo+"' ) ")

                return response.status(200).send({message: 'Se ha registrado al vehículo correctamente', vehiculo:vehiculo[0]})
            }

        } catch (error) { 
            
            console.log("No añadido ",error) 
        }
    }


    //Consultar
    async consultarVehiculos({request, params, response}){

        try {

            const vehiculo = await Database.raw("select id, chofer, oficial, bultos, placa, idproveedor, nota, fecha, horaentrada, horaingreso, horasalida, estado, estadoVehiculo from registrovehiculos where estado = 1 order by 1 desc;")

            return response.status(200).send({vehiculo:vehiculo[0]})
            
        } catch (error) {
            
            return error

        }
    }


    //Consultar por Id
    async consultarVehiculoId({request, params, response}){

        try {

            const {vehiculoId} =  request.params;
            const vehiculos = await Database.raw("select id, chofer, oficial, bultos, placa, idproveedor, nota, fecha, horaentrada, horaingreso, horasalida, estado, estadoVehiculo from registrovehiculos where id= '"+vehiculoId+"'")

            return response.status(200).send({vehiculos:vehiculos[0]})
            
        } catch (error) {
            
            return error
        }
    }


    //Actualizar
    async actualizarVehiculos({request, params, response}){

        try {

            const {vehiculoId} = request.params;
            let chofer = request.input('chofer').toUpperCase();
            let oficial = request.input('oficial').toUpperCase();
            let bultos = request.input('bultos');
            let placa = request.input('placa').toUpperCase();
            let idproveedor = request.input('idproveedor');
            let nota = request.input('nota').toUpperCase();
            let fecha = moment().format('YYYY-MM-DD');
            let horaentrada = moment().format('HH:mm:ss');
            let horaingreso = moment().format('HH:mm:ss');
            let horasalida = moment().format('HH:mm:ss');
            
            const vehiculo = await Database.raw("update registrovehiculos set chofer ='"+chofer+"', oficial='"+oficial+"', bultos='"+bultos+"', placa='"+placa+"',idproveedor='"+idproveedor+"', nota='"+nota+"', fecha='"+fecha+"', horaentrada='"+horaentrada+"', horaingreso='"+horaingreso+"', horasalida= '"+horasalida+"' where id='"+vehiculoId+"'")

            return response.status(200).send({message: 'Se ha actualizado al vehículo correctamente', vehiculo:vehiculo[0]})

        } catch (error) { 
            
            console.log("No actualizado ",error)
            
        }
    }

    //Actualizar Estado Vehiculo
    async actualizarEstadoVehiculo({request, params, response}){

        try {
            const {vehiculoId} =  request.params;
            let vh = []
            let horaingreso = moment().format('HH:mm:ss');
            let horasalida = moment().format('HH:mm:ss');
            const vehiculos = await Database.raw("select id, chofer, oficial, bultos, placa, idproveedor, nota, fecha, horaentrada, horaingreso, horasalida, estado, estadoVehiculo from registrovehiculos where id= '"+vehiculoId+"'")
            
            if(vehiculos.length>0){

                for (const v of vehiculos[0]) {  //[0: [{}, {}]
    
                    vh = v
                }
            } 

            if(vh.estadoVehiculo == 0){
                
                let estadoVehiculo = 1
                
                const vehiculo = await Database.raw("update registrovehiculos set horaingreso='"+horaingreso+"', estadoVehiculo='"+estadoVehiculo+"' where id='"+vehiculoId+"'")

                return response.status(200).send({message: 'Se ha actualizado la hora de ingreso y el estado del vehículo correctamente', vehiculo:vehiculo[0]})        
            
            }else{
                
                let estadoVehiculo = 2

                const vehiculo = await Database.raw("update registrovehiculos set horasalida='"+horasalida+"', estadoVehiculo='"+estadoVehiculo+"' where id='"+vehiculoId+"'")
             
                return response.status(200).send({message: 'Se ha actualizado la hora de salida correctamente', vehiculo:vehiculo[0]})
                
                
            
            }
            
        } catch (error) {
            
            console.log(error)

        }
    }


    //Eliminar
    async eliminarVehiculos({request, params, response}){

        try {

            let estado = 0;
            const {vehiculoId} = request.params;
            const vehiculo = await Database.raw("update registrovehiculos set estado='"+estado+"' where id='"+vehiculoId+"'")
                
            return response.status(200).send({message: 'Se ha eliminado al vehículo correctamente'})

        } catch (error) { 
            
            console.log("No eliminado ",error)
            
        }
    }
}
module.exports = VehiculosController