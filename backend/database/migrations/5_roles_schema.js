'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolesSchema extends Schema {
  up () {
    this.create('sysweb.roles', (table) => {
      table.increments()
      table.string('tipo_rol', 255).notNullable()
      table.string('descripcion', 255)
      table.timestamps()
    })
  }

  down () {
    this.drop('sysweb.roles')
  }
}

module.exports = RolesSchema
