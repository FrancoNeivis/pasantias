'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermisosUsuarioSchema extends Schema {
  up () {
    this.create('sysweb.permisos_usuarios', (table) => {
      table.increments()
      table.integer('users_id').unsigned().references('id').inTable('sysweb.users')
      table.integer('permisos_id').unsigned().references('id').inTable('sysweb.permisos')
      table.boolean('estado').defaultTo('false')
      table.timestamps()
    })
  }

  down () {
    this.drop('sysweb.permisos_usuarios')
  }
}

module.exports = PermisosUsuarioSchema
