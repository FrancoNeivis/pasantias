'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermisosSchema extends Schema {
  up () {
    this.create('sysweb.permisos', (table) => {
      table.increments()
      table.string('modulo')
      table.string('tipoobjeto')
      table.string('nombreobjeto')
      table.string('ruta')
      table.string('descripcion')
      table.string('codigo_menu')
      table.timestamps()
    })
  }

  down () {
    this.drop('sysweb.permisos')
  }
}

module.exports = PermisosSchema
