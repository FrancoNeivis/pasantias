'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolesUsersSchema extends Schema {
  up () {
    this.create('sysweb.roles_users', (table) => {
      table.increments()
      table.integer('users_id').unsigned().references('id').inTable('sysweb.users')
      table.integer('roles_id').unsigned().references('id').inTable('sysweb.roles')
      table.timestamps()
    })
  }

  down () {
    this.drop('sysweb.roles_users')
  }
}

module.exports = RolesUsersSchema
