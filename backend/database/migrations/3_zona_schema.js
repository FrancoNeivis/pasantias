'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ZonaSchema extends Schema {
  up () {
    this.create('calificacion.zonas', (table) => {
      table.increments("id_zonas")
      table.string('nombre', 150).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('calificacion.zonas')
  }
}

module.exports = ZonaSchema
