'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CalificacionSchema extends Schema {
  up () {
    this.create('calificacion.calificaciones', (table) => {
      table.increments("id_calificaciones")
      table.integer("id_zonas").unsigned().references("id_zonas").inTable("calificacion.zonas")
      table.integer("id_tipo_calificacion").unsigned().references("id_tipo_calificacion").inTable("calificacion.tipo_calificacion")
      table.string("comentario")
      table.string("numero_factura")
      table.integer("id_ctsucursales").unsigned().references("id").inTable("pgsistemas.ctsucursales")
      table.timestamps()
    })
  }

  down () {
    this.drop('calificacion.calificaciones')
  }
}

module.exports = CalificacionSchema
