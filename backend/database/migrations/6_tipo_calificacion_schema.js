'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoCalificacionSchema extends Schema {
  up () {
    this.create('calificacion.tipo_calificacion', (table) => {
      table.increments("id_tipo_calificacion")
      table.string("calificacion")
      table.timestamps()
    })
  }

  down () {
    this.drop('calificacion.tipo_calificacion')
  }
}

module.exports = TipoCalificacionSchema
