'use strict'

const Factory = use('Factory')
const Database = use('Database')

const User = use('App/Models/User')

class UserSeeder {
  async run () {
    const users = await Database.table('sysweb.users')
    console.log(users)
  
    await User.create({
      username: 'Andres Cobena',
      email: 'acobena@grupozurita.com.ec',
      password: 'pedrocobe'
    })
  
    await User.create({
      username: 'Admin Diverzu',
      email: 'webadmin@grupozurita.com.ec',
      password: 'Ticzurita*3175'
    })

    await User.create({
      username: 'Gustavo Rodriguez',
      email: 'grodriguez@grupozurita.com.ec',
      password: 'Grodriguez*2020'
    })

    await User.create({
      username: 'Miguel Zurita',
      email: 'miguelzurita@grupozurita.com.ec',
      password: 'Miguelzurita*2020'
    })

    await User.create({
      username: 'Edison Zurita',
      email: 'ezurita@grupozurita.com.ec',
      password: 'Ezurita*2020'
    })
    
    await User.create({
      username: 'Marisol Ganchozo',
      email: 'marisolg@grupozurita.com.ec',
      password: 'Marisolg*2020'
    })


  }

  
}

module.exports = UserSeeder