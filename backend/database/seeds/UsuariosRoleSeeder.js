'use strict'

const Factory = use('Factory')
const Database = use('Database')

const usuario = use('App/Models/RolesUser')


class UsuariosRoleSeeder {
  async run () {

      const rolesusers = await Database.table('sysweb.roles_users')
      console.log(rolesusers)
    
      await usuario.create({
        users_id: 1,
        roles_id: 1
      })
    
      await usuario.create({
        users_id: 2,
        roles_id: 1
      })
  
      await usuario.create({
        users_id: 3,
        roles_id: 1
      })

      await usuario.create({
        users_id: 4,
        roles_id: 1
      })

      await usuario.create({
        users_id: 5,
        roles_id: 1
      })
    

  }
}

module.exports = UsuariosRoleSeeder
