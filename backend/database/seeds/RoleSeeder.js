'use strict'

const Factory = use('Factory')
const Database = use('Database')

const Role = use('App/Models/Role')


class RoleSeeder {

  async run () {
    const role = await Database.table('sysweb.roles')
    console.log(role)
  
    await Role.create({
      tipo_rol: 'Administrador',
      descripcion: 'Rol de Administrador'
    })
  
    await Role.create({
      tipo_rol: 'Usuario',
      descripcion: 'Rol de usuario'
    })

  }
}

module.exports = RoleSeeder
