'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */

const Route = use('Route')

Route.on('/').render('home')



//Rutas de vehículos
Route.post('vehiculos/ingresarVehiculos', 'VehiculosController.ingresarVehiculos')

Route.get('vehiculos/consultarVehiculos', 'VehiculosController.consultarVehiculos')

Route.put('vehiculos/actualizarVehiculos/:vehiculoId', 'VehiculosController.actualizarVehiculos')

Route.put('vehiculos/eliminarVehiculos/:vehiculoId', 'VehiculosController.eliminarVehiculos')

Route.get('vehiculos/consultarVehiculoId/:vehiculoId', 'VehiculosController.consultarVehiculoId')

Route.put('vehiculos/actualizarEstadoVehiculo/:vehiculoId', 'VehiculosController.actualizarEstadoVehiculo')